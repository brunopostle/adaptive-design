FILE=urb

default: $(FILE).pdf

$(FILE).pdf: $(FILE).tex $(FILE).bib
	pdflatex $(FILE)
	bibtex $(FILE)
	pdflatex $(FILE)
	pdflatex $(FILE)

clean:
	rm -f *.log *.blg *.bbl *~ *.aux *.dvi *.pdf *.ps *.lbl *.out

.PHONY: clean default
